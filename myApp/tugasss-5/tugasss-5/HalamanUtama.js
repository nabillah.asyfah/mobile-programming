import * as React from 'react';
import { Text, View, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native';
import { Constants } from 'expo';

// You can import from local files
import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

export default class App extends React.Component {
  render() {
    return (
      <View style={{ 
        height: 100 + '%',
        width: 100 + '%',
        backgroundColor: 'rgb(115, 255, 216)',
        justifyContent: 'center',
        alignItems: 'center',

      }}> 
      <Image source = {require('./assets/iput.png') }
        style = {{ 
          backgroundColor: 'rgba(255, 255, 255, 0.5)',
          height: 88,
          width: 88,
          borderRadius: 44,
          marginTop: 18, 

      }}
       
   />

      <Text style =
      {{
        marginBottom: 0,
        fontSize: 35,
        color: "Black",

      }}
      >Welcome Back!
      </Text>

      <Text style =
      {{
        fontSize: 24,
        color: "Black",

      }}
      >
      </Text>

     

     <Text style =
      {{
        marginBottom:-10,
        fontSize:10,
        color: "black",

      }}
      >if you already have an account
      </Text>
      
      <TouchableOpacity style={{
        height: 40,
        width: 220,
        borderRadius: 25,
        fontSize: 16,
        padding: 15,
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        justifyContent: 'center',
        marginTop: 20,
        }}>
          <Text style={{
            color: 'rgba(0, 0, 0, 70)',
            fontWeight: "bold",
            fontSize: 14,
            textAlign: 'center'
          }}>LOG IN</Text>
      </TouchableOpacity>

       <Text style =
      {{
        marginTop:20,
        marginBottom:10,
        fontSize:11,
        color: "black",

      }}
      >OR
      </Text>

       <Text style =
      {{
        fontSize:10,
        color: "black",

      }}
      >Please select SIGN UP,
      </Text>

      <Text style =
      {{
        marginBottom:-10,
        fontSize:10,
        color: "black",

      }}
      >if you don't have an account
      
      </Text>
      
      <TouchableOpacity style={{
        height: 40,
        width: 220,
        borderRadius: 25,
        fontSize: 16,
        padding: 15,
        backgroundColor: 'rgba(255, 255, 255, 0.5)',
        justifyContent: 'center',
        marginTop: 20,
        }}>
          <Text style={{
            color: 'rgba(0, 0, 0)',
            fontWeight: "bold",
            fontSize: 14,
            textAlign: 'center'
          }}>SIGN UP</Text>
      </TouchableOpacity>

      </View>
    );
  }
}
