import React, { Component } from 'react';
import { Alert, Button, Text, TouchableOpacity, TextInput, View, StyleSheet } from 'react-native';

export default class App extends Component {

    state = {
      NIM: '',
      password: '',
    };
  
  
  onLogin() {
    const { NIM, password } = this.state;

    Alert.alert('Credentials', `NIM: ${NIM} + password: ${password}`);
  }

  render() {
    return (
      <View style={styles.container}>
      <Text style={styles.titleText}>Welcome Back!</Text>
        <Text style={styles.titleText}>Enter Personal details to your college account</Text>
        <TextInput
          value={this.state.NIM}
          keyboardType = 'NIM'
          onChangeText={(NIM) => this.setState({ NIM })}
          placeholder='NIM'
          placeholderTextColor = 'black'
          style={styles.input}
        />
        <TextInput
          value={this.state.password}
          onChangeText={(password) => this.setState({ password })}
          placeholder={'password'}
          secureTextEntry={true}
          placeholderTextColor = 'black'
          style={styles.input}
        />
        
     
        <TouchableOpacity
          style={styles.button}
          onPress={this.onLogin.bind(this)}
       >
         <Text style={styles.buttonText}> Sign Up / Login </Text>
       </TouchableOpacity>
        
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'aquamarine',
  },
  titleText:{
    fontFamily: 'Baskerville',
    fontSize: 24,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    alignItems: 'center',
    backgroundColor: 'powderblue',
    width: 200,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 25,
    marginBottom: 10,
  },
  buttonText:{
    fontFamily: 'Baskerville',
    fontSize: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  input: {
    width: 200,
    fontFamily: 'Baskerville',
    fontSize: 20,
    height: 44,
    padding: 10,
    borderWidth: 1,
    borderColor: 'white',
    marginVertical: 10,
  },
});